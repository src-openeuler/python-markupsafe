%global _description\
MarkupSafe implements a text object that escapes characters so it is safe to use in HTML and XML.\
Characters that have special meanings are replaced so that they display as the actual characters.\
This mitigates injection attacks, meaning untrusted user input can safely be displayed on a page.

Name:           python-markupsafe
Version:        2.1.5 
Release:        1
Summary:        Safely add untrusted strings to HTML/XML markup.
License:        BSD
URL:            https://pypi.org/project/MarkupSafe/
Source0:        https://files.pythonhosted.org/packages/source/M/MarkupSafe/MarkupSafe-%{version}.tar.gz

%description %_description

%package -n python3-markupsafe
Summary:        %{summary} 
BuildRequires:  python3-devel python3-setuptools gcc
%{?python_provide:%python_provide python3-markupsafe}

%description -n python3-markupsafe %_description

%package_help

%prep
%autosetup -n MarkupSafe-%{version}

%build
%py3_build


%install
%py3_install

%check
%{__python3} setup.py test


%files -n python3-markupsafe
%{python3_sitearch}/*.egg-info/
%exclude %{python3_sitearch}/markupsafe/_speedups.c
%{python3_sitearch}/markupsafe/

%files help
%doc README.rst

%changelog
* Wed Jun 12 2024 zeng liwen <zengliwen@kylinos.cn> - 2.1.5-1
- Update to 2.1.5
- Don’t use regular expressions for striptags, avoiding a performance issue
- Fix striptags not collapsing spaces

* Wed Jul 12 2023 sunhui <sunhui@kylinos.cn> - 2.1.3-1
- Update package to version 2.1.3

* Thu Jan 19 2023 chendh6 <chendonghui6@huawei.com> - 2.1.1-1
- upgrade to 2.1.1

* Sun Jul 24 2022 liksh <liks11@chinaunicom.com> - 2.1.0-1
- upgrade to 2.1.0 for openstack yoga

* Thu Nov 25 2021 liudabo <liudabo1@huawei.com> - 2.0.1-1
- upgrade version to 2.0.1

* Wed Jun 23 2021 yuanxin<yuanxin24@huawei.com> - 1.1.1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add buildrequires gcc

* Fri Oct 30 2020 wuchaochao <wuchaochao4@huawei.com> - 1.1.1-7
- Type:bufix
- CVE:NA
- SUG:NA
- DESC:remove python2

* Sat Jul 25 2020 tianwei <tianwei12@huawei.com> - 1.1.1-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to release 1.1.1

* Fri Oct 11 2019 yefei <yefei25@huawei.com> - 1.0-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:move author from doc to license

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0-2
- Package init
